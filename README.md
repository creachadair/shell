# shell

(deprecated)

This repository is no longer maintained. It remains here to avoid breaking
existing imports of `bitbucket.org/creachadair/shell`, but users should
consider switching to the new location,
<http://godoc.org/github.com/creachadair/mds/shell> .
Further improvements and fixes will be made there, not here.
